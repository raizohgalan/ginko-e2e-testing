<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SUTC001 - Sign Up Test Case</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>false</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f57b79c8-33ac-4d48-9227-4ccfb3db5fa9</testSuiteGuid>
   <testCaseLink>
      <guid>820017cf-d51f-43be-a698-657212d2f459</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1. Global Test Case/1. Launch The App</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e40b1549-84bd-4658-872a-9680bd6d7992</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/1. Global Test Case/2. Update the Active row where the test data will be fetch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b397c0b-83f2-471b-a375-d19c915d7202</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1. Global Test Case/3. Set Values for Global Variables</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7ccd11c-3a13-45d2-ba85-fe570cc57f3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/2. Onboarding/1. Register/SUTC001-1 Create Login Credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f296cc36-b012-4b0e-8240-3c5a26777f2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/2. Onboarding/1. Register/SUTC001-2 Create Ginko Personal Profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cec90838-bccb-4687-9bf1-885f2d6fb6f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2. Onboarding/1. Register/SUTC001-3 Add Work Profile in Personal Profile</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
