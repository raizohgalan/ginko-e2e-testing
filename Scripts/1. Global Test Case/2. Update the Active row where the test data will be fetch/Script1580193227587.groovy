import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.FileOutputStream as FileOutputStream
import java.io.IOException as IOException
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook

if(GlobalVariable.AppStatus == "True"){ 
	if (GlobalVariable.Onboarding_active_row == 0){
		'Install the app to your mobile phone'
		'the code bellow will get the current value of active_row'
		prev_row = findTestData('Ginko Accounts').getValue(18, 1)
		
		'Convert prev_row to integer so that you can perform addition'
		Integer x = Integer.parseInt(prev_row)
		
		'Perform addition and store the value into global variables so that you can access it into deffirent test case/suites'
		GlobalVariable.Onboarding_active_row = (x + 1)
	}
	'Open the file as write-only, if you planing to print the value it will trigger the error because FileInputStream is for writing only not for display the content of the file'
	FileInputStream fis = new FileInputStream('C:\\Users\\Lenovo\\Downloads\\Slack Files\\Ginko Automated Test Data.xlsx')
	
	'create variable for the use of selecting the sheet later'
	XSSFWorkbook workbook = new XSSFWorkbook(fis)
	
	'Select sheet name you cant also use number/index number of the sheet'
	XSSFSheet sheet = workbook.getSheet('Sheet1')
	
	'Select row by index number. It will select the first row'
	Row row = sheet.getRow(1)
	
	'Select cell'
	Cell cell = row.getCell(17)
	
	'Set the cell type'
	cell.setCellType(cell.CELL_TYPE_NUMERIC)
	
	'Update the cell value'
	cell.setCellValue(GlobalVariable.Onboarding_active_row)
	
	'Open the file again for ouput stream'
	FileOutputStream fos = new FileOutputStream('C:\\Users\\Lenovo\\Downloads\\Slack Files\\Ginko Automated Test Data.xlsx')
	
	'The code bellow will update the file contents'
	workbook.write(fos)
	
	'Close the file, it is recomended to close the file after using it ^_^'
	fos.close()
} 


