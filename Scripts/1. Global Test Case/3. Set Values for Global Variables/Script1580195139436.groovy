import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

prev_row = findTestData('Ginko Accounts').getValue(18, 1)

'Convert prev_row to integer so that you can perform addition'
Integer x = Integer.parseInt(prev_row)

'Perform addition and store the value into global variables so that you can access it into deffirent test case/suites'
GlobalVariable.Onboarding_active_row = x

'Set the name value'
GlobalVariable.Name = findTestData("Ginko Accounts").getValue(1, GlobalVariable.Onboarding_active_row)

'Set the password value'
GlobalVariable.Password = findTestData("Ginko Accounts").getValue(2, GlobalVariable.Onboarding_active_row)

'Set the valid email value'
GlobalVariable.Valid_Email = findTestData("Ginko Accounts").getValue(3, GlobalVariable.Onboarding_active_row)

'Set the invalid email 1 value'
GlobalVariable.Invalid_Email_1 = findTestData("Ginko Accounts").getValue(4, GlobalVariable.Onboarding_active_row)

'Set the invalid email 2 value'
GlobalVariable.Invalid_Email_2 = findTestData("Ginko Accounts").getValue(5, GlobalVariable.Onboarding_active_row)

'Set the invalid email 3 value'
GlobalVariable.Invalid_Email_3 = findTestData("Ginko Accounts").getValue(6, GlobalVariable.Onboarding_active_row)

'Set the address value'
GlobalVariable.Address = findTestData("Ginko Accounts").getValue(7, GlobalVariable.Onboarding_active_row)

'Set the birthday value'
GlobalVariable.Birthday = findTestData("Ginko Accounts").getValue(8, GlobalVariable.Onboarding_active_row)

'Set the email value'
GlobalVariable.Email = findTestData("Ginko Accounts").getValue(9, GlobalVariable.Onboarding_active_row)

'Set the facebook value'
GlobalVariable.Facebook = findTestData("Ginko Accounts").getValue(10, GlobalVariable.Onboarding_active_row)

'Set the fax value'
GlobalVariable.Fax = findTestData("Ginko Accounts").getValue(11, GlobalVariable.Onboarding_active_row)

'Set the hours value'
GlobalVariable.Hours = findTestData("Ginko Accounts").getValue(12, GlobalVariable.Onboarding_active_row)

'Set the linkedin value'
GlobalVariable.Linkedin = findTestData("Ginko Accounts").getValue(13, GlobalVariable.Onboarding_active_row)

'Set the mobile value'
GlobalVariable.Mobile = findTestData("Ginko Accounts").getValue(14, GlobalVariable.Onboarding_active_row)

'Set the phone value'
GlobalVariable.Phone = findTestData("Ginko Accounts").getValue(15, GlobalVariable.Onboarding_active_row)

'Set the twitter value'
GlobalVariable.Twitter = findTestData("Ginko Accounts").getValue(16, GlobalVariable.Onboarding_active_row)

'Set the website value'
GlobalVariable.Website = findTestData("Ginko Accounts").getValue(17, GlobalVariable.Onboarding_active_row)