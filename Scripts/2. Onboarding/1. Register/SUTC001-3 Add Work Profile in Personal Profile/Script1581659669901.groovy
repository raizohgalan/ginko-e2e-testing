import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

'[Step 1] Check if there are Work Profile that already added'
Mobile.verifyElementNotVisible(findTestObject('Onboarding/Work Profile/Buttons/Footer/Work and Personal Profile/Add New Work Profile'), 
    0)

'[Step 1] Check if there are Work Profile that already added'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Footer Buttons/Add Work Profile(Edit Mode)'), 
    0)

'[Step 1] Check if there are Work Profile that already added'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Footer Buttons/Profile Icon(Edit Mode)'), 
    0)

'[Step 2] Add new Work Profile by clicking the attache case with plus icon '
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Footer Buttons/Add Work Profile(Edit Mode)'), 0)

'[Step 3] Check if Email field exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Fields/Email Field/Email Field Container'), 0)

'[Step 3] Check if Email field exist(Validate if the email is equal to the email you inputed before)'
Mobile.verifyEqual(findTestObject('Onboarding/Work Profile/Fields/Email Field/Email Field Value'), GlobalVariable.Valid_Email, 
    FailureHandling.CONTINUE_ON_FAILURE)

'[Step 4] Check if privacy icon is visible to the header and in the bottom of email field'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Header/Private Privacy'), 0)

'[Step 5] Check if the default privacy is private'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Header/Private Privacy'), 0)

'[Step 6] Check there are upload button in the Profile picture and Back picture is visible\t'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Header/Upload Background Photo Button'), 0)

'[Step 6] Check there are upload button in the Profile picture and Back picture is visible\t'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Header/Upload Profile Photo Button'), 0)

'[Step 7] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Body/ADD VIDEO TO YOUR PROFILE Button'), 0)

'[Step 7] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Body/Next button'), 0)

'[Step 7] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Body/Add New Field button'), 0)

'[Step 7] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Footer/Work and Personal Profile/Personal Profile'), 
    0)

'[Step 7] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Footer/Work and Personal Profile/Work Profile 1'), 
    0)

'[Step 7] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Footer/Work and Personal Profile/Add New Work Profile'), 
    0)

'[Step 8] Try to change the privacy of your profile to private by clicking the Eye icon on the header'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Header/Private Privacy'), 0)

'[Step 9] Close the modal by clicking the Ok button'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Privacy Modals/Private Modal/Privacy Modal Container'), 
    0)

'[Step 9] Close the modal by clicking the Ok button'
Mobile.tap(findTestObject('Onboarding/Work Profile/Privacy Modals/Private Modal/Close Button'), 0)

'[Step 10] Check if the upload icon button is clickable in profile picture container'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Header/Upload Profile Photo Button'), 0)

'[Step 11] Go back to Ginko app without choosing any image in the gallery, click hardware back to return in the app\t'
Mobile.pressBack()

'[Step 12] Check if the upload icon button is clickable in profile background picture container'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Header/Upload Background Photo Button'), 0)

'[Step 13] Go back to Ginko app without choosing any image in the gallery, click hardware back to return in the app'
Mobile.pressBack()

'[Step 14] Upload Profile Picture by clicking the upload icon that align in your profile picture\t'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Header/Upload Profile Photo Button'), 0)

'[Step 15] Select your desire profile photo by clicking it and submit by clicking the check button above'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Photos/Photo 2'), 0)

'[Step 15] Select your desire profile photo by clicking it and submit by clicking the check button above'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Submit Button'), 0)

'[Step 16] Check if the photo you selected is visible in profile picture\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Profile and Background Photo/Profile Photo'), 0)

'[Step 17] Upload Background Photo by clicking the upload icon that align in your Background Photo'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Header/Upload Background Photo(With profile picture)'), 0)

'[Step 18] Select your desire Background Photo by clicking it  and submit by clicking the check button above'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Photos/Photo 3'), 0)

'[Step 18] Select your desire Background Photo by clicking it  and submit by clicking the check button above'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Submit Button'), 0)

'[Step 19] Check if the photo you selected is visible in Background Photo'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Profile and Background Photo/Background Photo'), 0)

'[Step 20] Add Video to your profile by clicking the Add Video to your Profile button'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Body/ADD VIDEO TO YOUR PROFILE Button'), 0)

'[Step 21] Check if there are record and upload button'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Buttons/Record Icon Button'), 
    0)

'[Step 21] Check if there are record and upload button'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Buttons/Upload Icon Button'), 
    0)

'[Step 22] Select Upload button'
Mobile.tap(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Buttons/Upload Icon Button'), 0)

'[Step 23] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/Menu'), 20)

'[Step 23] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/Videos'), 20)

'[Step 23] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/ScreenCapture'), 20)

'[Step 23] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/Selected Video'), 20)

'[Step 24] Check if there a video added to your profile'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Video Uploaded/Video Container'), 
    0)

'[Step 25] Remove Video Profile by clicking the trash icon in the video'
Mobile.tap(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Video Uploaded/Delete Button'), 0)

'[Step 26] Check if the button "Add Video to your Profile" is visible'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Buttons/Body/ADD VIDEO TO YOUR PROFILE Button'), 0)

'[Step 27] Add Video to your profile by clicking the Add Video to your Profile button'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Body/ADD VIDEO TO YOUR PROFILE Button'), 0)

'[Step 28] Select Record button'
Mobile.tap(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Buttons/Record Icon Button'), 0)

'[Step 29] Start Recording your video profile by clicking the the capture button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Start Video/Samsung A50/Record'), 5)

'[Step 30] Stop the recording'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Capturing Video/Samsung A50/Stop'), 10)

'[Step 31] Click done  button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video Preview/Samsung A50/OK'), 20)

'[Step 32] Check if there a video added to your profile'
Mobile.verifyElementVisible(findTestObject('Onboarding/Work Profile/Add Video To Your Profile/Video Uploaded/Video Container'), 
    0)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 33] Add new field by clicking the Plus icon in the right bottom of the page'
Mobile.tap(findTestObject('Onboarding/Work Profile/Buttons/Body/Add New Field button'), 0)

