import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Get the y position of the link text'
int Register_Here_y_position = Mobile.getElementTopPosition(findTestObject('Login/Buttons/Dont have an account Register here.'), 
    5)

'Get the x position of the link text'
int Register_Here_x_position = Mobile.getElementLeftPosition(findTestObject('Login/Buttons/Dont have an account Register here.'), 
    5)

'Get the width of link text'
int Register_Here_width = Mobile.getElementWidth(findTestObject('Login/Buttons/Dont have an account Register here.'), 5)

'Add the x position value and the width then minus 50 so that the link could be click'
Register_Here_x_position = ((Register_Here_x_position + Register_Here_width) - 50)

'Add 10px to the y position of link text'
Register_Here_y_position = (Register_Here_y_position + 10)

'[Step 2] Click Sign Up Here button'
Mobile.tapAtPosition(Register_Here_x_position, Register_Here_y_position, FailureHandling.STOP_ON_FAILURE)

'[Step 3] Check if the Name Field exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Text Field/Name - Text Field'), 0, FailureHandling.STOP_ON_FAILURE)

'[Step 3] Check if the Email FIeld exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Text Field/Email'), 0, FailureHandling.STOP_ON_FAILURE)

'[Step 3] Check if the Create Password Field exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Text Field/Create Password'), 0, FailureHandling.STOP_ON_FAILURE)

'[Step 4] Check if the Join Now button exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Buttons/JOIN NOW'), 0, FailureHandling.STOP_ON_FAILURE)

'[Step 4] Check if the  Terms and Privacy Policy exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Links/By joining you agree to our Terms and Privacy Policy - Link'), 
    0, FailureHandling.STOP_ON_FAILURE)

int Login_Here_y_position = Mobile.getElementTopPosition(findTestObject('Onboarding/1. Register/Buttons/Login here.'), 5)

int Login_Here_x_position = Mobile.getElementLeftPosition(findTestObject('Onboarding/1. Register/Buttons/Login here.'), 
    5)

int Login_Here_width = Mobile.getElementWidth(findTestObject('Onboarding/1. Register/Buttons/Login here.'), 5)

Login_Here_x_position = ((Login_Here_x_position + Login_Here_width) - 20)

Login_Here_y_position = (Login_Here_y_position + 10)

'[Step 5] Try to go back in Login page by clicking the Login Here button/Hardware back'
Mobile.tapAtPosition(Login_Here_x_position, Login_Here_y_position)

Mobile.waitForElementPresent(findTestObject('Login/Buttons/Dont have an account Register here.'), 10)

'[Step 2] Click Sign Up Here button'
Mobile.tapAtPosition(Register_Here_x_position, Register_Here_y_position, FailureHandling.STOP_ON_FAILURE)

'[Step 11] Submit the form with empty fields'
Mobile.tap(findTestObject('Onboarding/1. Register/Buttons/JOIN NOW'), 10, FailureHandling.CONTINUE_ON_FAILURE)

'[Step 11][Name] It should submit the form and must return error message for empty fields'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Labels/Empty Field Error Message/Error for empty fieldsName Please provide your name.'), 
    10, FailureHandling.STOP_ON_FAILURE)

'[Step 11][Email] It should submit the form and must return error message for empty fields'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Labels/Empty Field Error Message/Error for empty fieldsEmail Please provide your email address.'), 
    10, FailureHandling.STOP_ON_FAILURE)

'[Step 11][Password] It should submit the form and must return error message for empty fields'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Labels/Empty Field Error Message/Error for empty fieldsPassword Please provide your password.'), 
    10, FailureHandling.STOP_ON_FAILURE)

'[Step 12] Enter text in Name field'
Mobile.sendKeys(findTestObject('Onboarding/1. Register/Text Field/Name - Text Field'), GlobalVariable.Name, FailureHandling.STOP_ON_FAILURE)

'[Step 13] Enter invalid email in Email field'
Mobile.sendKeys(findTestObject('Onboarding/1. Register/Text Field/Email'), GlobalVariable.Invalid_Email_1, FailureHandling.STOP_ON_FAILURE)

'[Step 14] Enter text in Password field'
Mobile.sendKeys(findTestObject('Onboarding/1. Register/Text Field/Create Password'), GlobalVariable.Password, FailureHandling.STOP_ON_FAILURE)

'[Step 15] Submit the form with invalid email'
Mobile.tap(findTestObject('Onboarding/1. Register/Buttons/JOIN NOW'), 10, FailureHandling.STOP_ON_FAILURE)

'Verify if the modal showed up'
if (Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Alert Modal/Alert Modal'), 10, FailureHandling.OPTIONAL)) {
    try {
        'Close the modal'
        Mobile.tap(findTestObject('Onboarding/1. Register/Alert Modal/OK - button'), 5, FailureHandling.STOP_ON_FAILURE)
    }
    catch (Exception e) {
        'Close the modal'
        Mobile.tap(findTestObject('Onboarding/1. Register/Alert Modal/OK - button'), 5, FailureHandling.STOP_ON_FAILURE)
    } 
}

'[Step 15][Email] Error message for invalid email showed up after submitting the form\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Labels/Empty Field Error Message/Error for Invalid Email  Please provide a valid email address.'), 
    10, FailureHandling.STOP_ON_FAILURE)

'[Step 16] Enter invalid email in Email field'
Mobile.sendKeys(findTestObject('Onboarding/1. Register/Text Field/Email'), GlobalVariable.Invalid_Email_2, FailureHandling.STOP_ON_FAILURE)

'[Step 17] Submit the form with invalid email'
Mobile.tap(findTestObject('Onboarding/1. Register/Buttons/JOIN NOW'), 10, FailureHandling.STOP_ON_FAILURE)

'Verify if the modal showed up'
if (Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Alert Modal/Alert Modal'), 10, FailureHandling.OPTIONAL)) {
    try {
        'Close the modal'
        Mobile.tap(findTestObject('Onboarding/1. Register/Alert Modal/OK - button'), 5, FailureHandling.STOP_ON_FAILURE)
    }
    catch (Exception e) {
        'Close the modal'
        Mobile.tap(findTestObject('Onboarding/1. Register/Alert Modal/OK - button'), 5, FailureHandling.STOP_ON_FAILURE)
    } 
}

'[Step 17][Email] Error message for invalid email showed up after submitting the form\r\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Labels/Empty Field Error Message/Error for Invalid Email  Please provide a valid email address.'), 
    10, FailureHandling.STOP_ON_FAILURE)

'[Step 18] Enter invalid email in Email field'
not_run: Mobile.sendKeys(findTestObject('Onboarding/1. Register/Text Field/Email'), GlobalVariable.Invalid_Email_3, FailureHandling.STOP_ON_FAILURE)

'[Step 19] Submit the form with invalid email'
not_run: Mobile.tap(findTestObject('Onboarding/1. Register/Buttons/JOIN NOW'), 30, FailureHandling.STOP_ON_FAILURE)

'Verify if the modal showed up'
not_run: Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Alert Modal/Alert Modal'), 60)

not_run: try {
    'Close the modal'
    Mobile.tap(findTestObject('Onboarding/1. Register/Alert Modal/OK - button'), 5, FailureHandling.STOP_ON_FAILURE)
}
catch (Exception e) {
    'Close the modal'
    Mobile.tap(findTestObject('Onboarding/1. Register/Alert Modal/OK - button'), 5, FailureHandling.STOP_ON_FAILURE)
} 

'[Step 19][Email] Error message for invalid email showed up after submitting the form\r\n'
not_run: Mobile.verifyElementVisible(findTestObject('Onboarding/1. Register/Labels/Empty Field Error Message/Error for Invalid Email  Please provide a valid email address.'), 
    30, FailureHandling.STOP_ON_FAILURE)

'[Step 20] Enter valid Email address'
Mobile.sendKeys(findTestObject('Onboarding/1. Register/Text Field/Email'), GlobalVariable.Valid_Email, FailureHandling.STOP_ON_FAILURE)

'[Step 21] Submit the form with valid email'
Mobile.tap(findTestObject('Onboarding/1. Register/Buttons/JOIN NOW'), 10, FailureHandling.STOP_ON_FAILURE)

'[Step 21] After submitting the form it redirects to Welcome Screen'
Mobile.verifyElementVisible(findTestObject('Onboarding/2. Welcome Screen/Labels/Welcome to - Label'), 10)

'[Step 22]Go to Profile Creation by clicking the LET\'S START button'
not_run: Mobile.tap(findTestObject('Onboarding/2. Welcome Screen/Buttons/LETS START'), 0)

