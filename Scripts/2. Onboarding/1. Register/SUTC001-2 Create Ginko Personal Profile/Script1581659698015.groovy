import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Onboarding/2. Welcome Screen/Buttons/LETS START'), 0)

'[Step 1] Select Personal Profile'
Mobile.tap(findTestObject('Onboarding/3. I would like to create/Buttons/Personal Profile Icon'), 0)

'[Step 2] Check if Email field exist'
Mobile.verifyElementExist(findTestObject('Onboarding/4. Personal Profile/Add New Field/Email Field Objects/Email Field Container'), 
    5, FailureHandling.OPTIONAL)

'[Step 3] Check if privacy icon is visible to the header and in the bottom of email field'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Public Privacy'), 0)

'[Step 3] Check if privacy icon is visible to the header and in the bottom of email field'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Public Privacy - Body'), 0)

'[Step 4] Check if the default privacy is public'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Public Privacy'), 0)

//// Create the base object
//TestObject uploadBackgroundPhoto = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//*[@class = 'android.widget.TextView' and (text() = '' or . = '')]", true)
'[Step 5] Check there are upload button in the Profile picture and Back picture is visible'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Upload Background Photo Icon (Empty profile photo)'), 
    5)

'[Step 5] Check there are upload button in the Profile picture and Back picture is visible'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Upload Profile Icon'), 5)

'[Step 6] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Add Video To Your Profile - Button'), 
    5)

'[Step 6] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Add Work Profile - Button Icon'), 5)

Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 6] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Add new field'), 5)

'[Step 6] Check if the Add Video To your Profile, Next, Plus Icon, Profile Icon and Add Work Profile icon button is exist'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Next'), 5)

Mobile.scrollToText('Personal Profile Setup', FailureHandling.STOP_ON_FAILURE)

'[Step 7] Try to change the privacy of your profile to private by clicking the Eye icon on the header'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Public Privacy'), 5)

'[Step 7] Eye icon should be clickable and must show Modal after clicking it'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Privacy Modal/Privacy Modal'), 5)

'[Step 8] Close the modal by clicking the Ok button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Privacy Modal/Close Modal'), 5)

'[Step 9]Check if the upload icon button is clickable in profile picture container'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Upload Profile Icon'), 10)

'Check if the the Storage permission is visible'
if (Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Photo Permission/Photo Permission'), 5, FailureHandling.OPTIONAL)) {
    'Allow the Storage permission'
    if (Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Photo Permission/ALLOW'), 5, FailureHandling.OPTIONAL)) {
        Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Photo Permission/ALLOW'), 20, FailureHandling.STOP_ON_FAILURE)
    } else {
        Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Photo Permission/Allow (1)'), 20, FailureHandling.STOP_ON_FAILURE)
    }
}

'[Step 10] Go back to Ginko app without choosing any image in the gallery, click hardware back to return in the app'
Mobile.pressBack()

'Check Add Video To Your Profile is visible that means it return to the profile setup'
if (Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Add Video To Your Profile - Button'), 
    5, FailureHandling.OPTIONAL)) {
} else {
    '[Step 10] Go back to Ginko app without choosing any image in the gallery, click hardware back to return in the app'
    Mobile.pressBack()
}

'[Step 11]Check if the upload icon button is clickable in background photo container'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Upload Background Photo Icon (Empty profile photo)'), 
    20)

'[Step 12] Go back to Ginko app without choosing any image in the gallery, click hardware back to return in the app'
Mobile.pressBack()

'Check Add Video To Your Profile is visible that means it return to the profile setup'
if (Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Buttons/Add Video To Your Profile - Button'), 
    5, FailureHandling.OPTIONAL)) {
} else {
    '[Step 12] Go back to Ginko app without choosing any image in the gallery, click hardware back to return in the app'
    Mobile.pressBack()
}

'[Step 13] Upload Profile Picture by clicking the upload icon that align in your profile picture'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Upload Profile Icon'), 10)

'[Step 14] Select your desire profile photo by clicking it and submit by clicking the check button above'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Photos/Photo 1'), 5)

'[Step 14] Select your desire profile photo by clicking it and submit by clicking the check button above'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Submit Button'), 15)

'Use try catch so that it will force the test to press the hardware back button if it will fail it will try again to press back button'
try {
    '[Step 15] Check if the photo you selected is visible in profile picture\r\n'
    Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Photos/Profile Photo'), 15)
}
catch (Exception e) {
    'Add delay to skip the loading screen of the app'
    Mobile.delay(10, FailureHandling.CONTINUE_ON_FAILURE)
} 

'[Step 16] Upload Background Photo by clicking the upload icon that align in your Background Photo'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Upload Background Photo Icon (with profile photo)'), 5)

'[Step 17] Select your desire Background Photo by clicking it  and submit by clicking the check button above\r\n'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Photos/Photo 3'), 5)

'[Step 17] Select your desire Background Photo by clicking it  and submit by clicking the check button above\r\n'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Submit Button'), 15)

'Use try catch so that it will take time to detect if the photo is uploaded'
try {
    '[Step 18] Check if the photo you selected is visible in Background Photo'
    Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Upload Photos/Photos/Background Photo'), 
        15)
}
catch (Exception e) {
    'Add delay to skip the loading screen of the app'
    Mobile.delay(10, FailureHandling.CONTINUE_ON_FAILURE)
} 

'[Step 19] Add Video to your profile by clicking the Add Video to your Profile button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video uploaded/ADD VIDEO TO YOUR PROFILE'), 
    5)

'[Step 20] Check if there are record and upload button'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Buttons/Record Button'), 5)

'[Step 20] Check if there are record and upload button'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Buttons/Upload Button'), 5)

'[Step 21] Select Upload button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Buttons/Upload Button'), 5)

'[Step 22] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/Menu'), 20)

'[Step 22] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/Videos'), 20)

'[Step 22] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/ScreenCapture'), 20)

'[Step 22] Select video you want to add in your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Upload Video/Samsung A50/Selected Video'), 20)

'[Step 23] Check if there a video added to your profile\r\n'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video uploaded/Video Container'), 
    20)

'[Step 24] Remove Video Profile by clicking the trash icon in the video'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video uploaded/Delete button'), 10)

'[Step 25] Check if the button "Add Video to your Profile" is visible'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video uploaded/ADD VIDEO TO YOUR PROFILE'), 
    5)

'[Step 26] Add Video to your profile by clicking the Add Video to your Profile button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video uploaded/ADD VIDEO TO YOUR PROFILE'), 
    5)

'[Step 27] Select Record button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Buttons/Record Button'), 20)

'Check if the the Camera permission is visible'
if (Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Camera Permission/Camera Permission Modal'), 
    5, FailureHandling.OPTIONAL)) {
    'Allow the camera permission'
    if (Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Camera Permission/ALLOW'), 
        5, FailureHandling.OPTIONAL)) {
        Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Camera Permission/ALLOW'), 10, FailureHandling.STOP_ON_FAILURE)
    } else {
        Mobile.tap(findTestObject('Object Repository/Onboarding/4. Personal Profile/Photo Permission/Allow (1)'), 20, FailureHandling.STOP_ON_FAILURE)
    }
}

'[Step 28] Start Recording your video profile by clicking the the capture button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Start Video/Samsung A50/Record'), 5)

'[Step 29] Stop the recording'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Capturing Video/Samsung A50/Stop'), 10)

'[Step 30] Click done  button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video Preview/Samsung A50/OK'), 20)

'[Step 31] Check if there a video added to your profile'
Mobile.verifyElementVisible(findTestObject('Onboarding/4. Personal Profile/Add Video Profile/Video uploaded/Video Container'), 
    20)

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Email Field/Email Field Edit Button'), 
    5)

Mobile.sendKeys(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Email Field/Email Field Editable'), 
    GlobalVariable.Valid_Email)

'It should hide the Keyboard'
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Email Field/Email Field Icon'), 
    5)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 32] Add new field by clicking the Plus icon in the right bottom of the page\r\n'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Add new field'), 5)

'[Step 33] Click close button in the right bottom of the page'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Add Fields/Close'), 5)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 34] Add new field by clicking the Plus icon in the right bottom of the page'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Add new field'), 5)

'[Step 35] Click Mobile to add mobile field on your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Add Fields/Add Mobile - label'), 5)

//Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Mobile Field/Mobile Field Edit Button'), 
//    5)
'[Step 36] Enter your desire mobile number in Mobile Field (Click Edit button to make the field editable)'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Mobile Field/Mobile Field Edit Button'), 
    5)

'[Step 36] Enter your desire mobile number in Mobile Field'
Mobile.sendKeys(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Mobile Field/Mobile Field Editable'), 
    GlobalVariable.Mobile)

'It should hide the Keyboard'
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Mobile Field/Mobile Field Icon'), 
    10)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 37] Add new field by clicking the Plus icon in the right bottom of the page'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Add new field'), 5)

'[Step 38] Click Address to add address field on your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Add Fields/Add Address - label'), 5)

'[Step 39] Enter your address in Address Field (Click Edit button to make the field editable)'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Address Field/Address Field Edit Button'), 
    5)

'[Step 39] Enter your address in Address Field'
Mobile.sendKeys(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Address Field/Address Field Editable'), 
    GlobalVariable.Address)

'It should hide the Keyboard'
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Address Field/Address Field Icon'), 
    10)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 40] Add new field by clicking the Plus icon in the right bottom of the page'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Add new field'), 5)

'[Step 41] Click Birthday to add birthday field on your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Add Fields/Add Birthday - label'), 5)

'[Step 42] Enter your Birthday Field (Click edit button to make the field editable)'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Birthday Field/Birthday Field Edit Button'), 
    5)

'[Step 42] Enter your Birthday Field'
Mobile.sendKeys(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Birthday Field/Birthday Field Editable'), 
    GlobalVariable.Birthday)

'It should hide the Keyboard'
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Birthday Field/Birthday Field Icon'), 
    10)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 43] Add new field by clicking the Plus icon in the right bottom of the page'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Add new field'), 5)

'[Step 44] Click Facebook to add facebook  field on your profile'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Add Fields/Add Facebook - label'), 5)

'[Step 45] Enter your Facebook link in the Field (Click edit button to make the field editable)'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Facebook Field/Facebook Field Edit Button'), 
    5)

'[Step 45] Enter your Facebook link in the Field'
Mobile.sendKeys(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Facebook Field/Facebook Field Editable'), 
    GlobalVariable.Facebook)

'It should hide the Keyboard'
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Facebook Field/Facebook Field Icon'), 
    10)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 46] Submit the form by clicking the Next button'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Buttons/Next Button (Added Fields)'), 5)

'[Step 47] Check if all the data you intput in the form is visible and correct'
Mobile.verifyElementText(findTestObject('Onboarding/5. Preview Profile/Fields/Email Field/Email Field Value'), GlobalVariable.Valid_Email)

'[Step 47] Check if all the data you intput in the form is visible and correct'
Mobile.verifyElementText(findTestObject('Onboarding/5. Preview Profile/Fields/Mobile Field/Mobile Field Value'), GlobalVariable.Mobile)

'[Step 47] Check if all the data you intput in the form is visible and correct'
Mobile.verifyElementText(findTestObject('Onboarding/5. Preview Profile/Fields/Address Field/Address Field Value'), GlobalVariable.Address)

'[Step 47] Check if all the data you intput in the form is visible and correct'
Mobile.verifyElementText(findTestObject('Onboarding/5. Preview Profile/Fields/Birthday Field/Birthday Field Value'), GlobalVariable.Birthday)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('DONE', FailureHandling.STOP_ON_FAILURE)

'[Step 47] Check if all the data you intput in the form is visible and correct'
Mobile.verifyElementText(findTestObject('Onboarding/5. Preview Profile/Fields/Facebook Field/Facebook Field Value'), GlobalVariable.Facebook)

'[Step 48] Edit/Remove the field by clicking the Edit button'
Mobile.tap(findTestObject('Onboarding/5. Preview Profile/Buttons/Profile Edit Button'), 5)

'Scroll the page so that i can click the Add field button'
Mobile.scrollToText('NEXT', FailureHandling.STOP_ON_FAILURE)

'[Step 49] Remove Facebook field by clicking the right trash icon in Facebook field'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Facebook Field/Facebook Field Delete Button(Edit Mode)'), 
    5)

'[Step 50] Edit Address field'
Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Address Field/Address Field Edit Button(Edit Mode)'), 
    5)

'Change the value of address in global variables'
GlobalVariable.Address = ('Jaro, ' + GlobalVariable.Address)

'[Step 51] Change Address'
Mobile.sendKeys(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Address Field/Address Field Editable(Edit Mode)'), 
    GlobalVariable.Address)

'It should hide the Keyboard'
Mobile.hideKeyboard()

Mobile.tap(findTestObject('Onboarding/4. Personal Profile/Add New Field/Personal Profile Fields/Address Field/Address Field Icon(Edit Mode)'), 
    10)

